﻿Imports System.Data.OleDb


Public Class inForm
    Dim dbPath As String = Application.StartupPath & "\Database15.accdb"
    Dim connectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0; DATA SOURCE = " & dbPath

    Private Function performQuery(ByVal connectionString As String, ByVal sqlCommand As String) As OleDb.OleDbDataReader
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()
        Dim dbDataReader As OleDb.OleDbDataReader = Nothing

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbConnection.Open()
            dbDataReader = dbCommand.ExecuteReader
            Return dbDataReader
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

        Return Nothing
    End Function

    Private Function performNonQuery(ByVal connectionString As String, ByVal sqlCommand As String) As Boolean
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbCommand.Connection.Open()
            dbCommand.ExecuteNonQuery()
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End Try

        Return True
    End Function

    Private Sub subBtn_Click(sender As Object, e As EventArgs) Handles subBtn.Click
        If txtName.Text.Trim.Length = 0 Then
            MessageBox.Show("Please type your Name.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If txtAddress.Text.Trim.Length = 0 Then
            MessageBox.Show("Please type your Address.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If txtContact.Text.Trim.Length = 0 Then
            MessageBox.Show("Please type your Contaact number.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If txtNum.Text.Trim.Length = 0 Then
            MessageBox.Show("Please type the number of days you will borrow the clothe.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        Dim dbDataReader As OleDb.OleDbDataReader = Nothing
        Dim Fu As String
        Dim sqlCommand As String = "SELECT * FROM Table1 WHERE Contact = '" & txtContact.Text & "'"
        dbDataReader = performQuery(connectionString, sqlCommand)
        If dbDataReader.HasRows = 0 Then
            sqlCommand = "SELECT * FROM Table1 WHERE FullName = '" & txtName.Text & "'"
            dbDataReader = performQuery(connectionString, sqlCommand)
            If dbDataReader.HasRows = 0 Then
                sqlCommand = "INSERT INTO Table1 (FullName , Address, Contact, NumberOfDays) VALUES ('" & txtName.Text & "', '" & txtAddress.Text & "', '" & txtContact.Text & "', '" & txtNum.Text & "')"
                If performNonQuery(connectionString, sqlCommand) Then
                    MessageBox.Show("New Contacts successfully added.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    txtName.Clear()
                    txtAddress.Clear()
                    txtContact.Clear()
                    txtNum.Clear()
                    Me.Close()
                    Exit Sub

                Else
                    MessageBox.Show("Unable to create new contact.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    txtName.Focus()
                    Exit Sub
                End If
            Else
                MessageBox.Show("Name aleady existing.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtName.Focus()
                Exit Sub
            End If
        Else
            MessageBox.Show("Contact number already existing.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtContact.Focus()
            Exit Sub

        End If



    End Sub
    Private Sub txtContact_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtContact.KeyPress
        If (Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57) Or Asc(e.KeyChar) = 8 Then
            e.Handled = False
        Else
            MessageBox.Show("Please enter numbers only")
            e.Handled = True
        End If
    End Sub

    Private Sub txtNum_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNum.KeyPress
        If (Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57) Or Asc(e.KeyChar) = 8 Then
            e.Handled = False
        Else
            MessageBox.Show("Please enter numbers only")
            e.Handled = True
        End If
    End Sub
End Class
